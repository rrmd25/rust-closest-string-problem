use std::time::Instant;

use closest_string_problem::csp::{csp_brute_force, csp_fixed_distance};

fn main() {
    let list: Vec<String> = vec![
        "AAACCCBBD".to_string(),
        "BBAACCBBD".to_string(),
        "AACCAACDD".to_string(),
        "DDAADDAAC".to_string(),
        "CCDDAACCA".to_string(),
        "AACAADDCC".to_string(),
        "CCAACCDDA".to_string(),
        "AADDAACCB".to_string(),
        "BABCCADDA".to_string(),
        "CCACADDBA".to_string(),
        "BBACADABA".to_string(),
    ];
    let start = Instant::now();
    let result_option = csp_fixed_distance(&list, 6);
    let duration = start.elapsed();
    assert_ne!(result_option, None);
    println!("Time elapsed is: {:?}", duration);
}
