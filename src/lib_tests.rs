#[cfg(test)]
mod timing_test {
    use std::time::Instant;

    use crate::csp::{csp_brute_force, csp_d_algorithm, csp_fixed_distance};

    #[test]
    fn test_csp_brute_force_timed_1_n4_l8_d5() {
        let list: Vec<String> = vec![
            "ACDACDDC".to_string(),
            "BBBDADBD".to_string(),
            "CDAADAAB".to_string(),
            "ABDCCDAD".to_string(),
        ];
        let start = Instant::now();
        let result_option = csp_brute_force(&list);
        let duration = start.elapsed();
        println!("Time elapsed is: {:?}", duration);
        assert_ne!(result_option, None);
    }
    #[test]
    fn test_csp_brute_force_timed_2_n5_l8_d5() {
        let list: Vec<String> = vec![
            "ACDACDDC".to_string(),
            "BBBDADBD".to_string(),
            "CDAADAAB".to_string(),
            "ABDCCDAD".to_string(),
            "BADCCDAD".to_string(),
        ];
        let start = Instant::now();
        let result_option = csp_brute_force(&list);
        let duration = start.elapsed();
        println!("Time elapsed is: {:?}", duration);
        assert_ne!(result_option, None);
    }
    #[test]
    fn test_csp_brute_force_timed_3_n6_l8_d5() {
        let list: Vec<String> = vec![
            "ACDACDDC".to_string(),
            "BBBDADBD".to_string(),
            "CDAADAAB".to_string(),
            "ABDCCDAD".to_string(),
            "BADCCDAD".to_string(),
            "CAABDBAA".to_string(),
        ];
        let start = Instant::now();
        let result_option = csp_brute_force(&list);
        let duration = start.elapsed();
        println!("Time elapsed is: {:?}", duration);
        assert_ne!(result_option, None);
    }
    #[test]
    fn test_csp_brute_force_timed_4_n7_l8_d5() {
        let list: Vec<String> = vec![
            "ACDACDDC".to_string(),
            "BBBDADBD".to_string(),
            "CDAADAAB".to_string(),
            "ABDCCDAD".to_string(),
            "BADCCDAD".to_string(),
            "CAABDBAA".to_string(),
            "ADBBDBAB".to_string(),
        ];
        let start = Instant::now();
        let result_option = csp_brute_force(&list);
        let duration = start.elapsed();
        println!("Time elapsed is: {:?}", duration);
        assert_ne!(result_option, None);
    }
    #[test]
    fn test_csp_brute_force_timed_5_n8_l8_d5() {
        let list: Vec<String> = vec![
            "ACDACDDC".to_string(),
            "BBBDADBD".to_string(),
            "CDAADAAB".to_string(),
            "ABDCCDAD".to_string(),
            "BADCCDAD".to_string(),
            "CAABDBAA".to_string(),
            "ADBBDBAB".to_string(),
            "ACABBAAA".to_string(),
        ];
        let start = Instant::now();
        let result_option = csp_brute_force(&list);
        let duration = start.elapsed();
        println!("Time elapsed is: {:?}", duration);
        assert_ne!(result_option, None);
    }

    #[test]
    fn test_csp_d_algorithm_timed_1_n4_l8_d5() {
        let list: Vec<String> = vec![
            "ACDACDDC".to_string(),
            "BBBDADBD".to_string(),
            "CDAADAAB".to_string(),
            "ABDCCDAD".to_string(),
        ];
        let distance = 5;
        let start = Instant::now();
        let result_option = csp_d_algorithm(&list, distance);
        let duration = start.elapsed();
        println!("Time elapsed is: {:?}", duration);
        assert_ne!(result_option, None);
    }
    #[test]
    fn test_csp_d_algorithm_timed_2_n5_l8_d5() {
        let list: Vec<String> = vec![
            "ACDACDDC".to_string(),
            "BBBDADBD".to_string(),
            "CDAADAAB".to_string(),
            "ABDCCDAD".to_string(),
            "BADCCDAD".to_string(),
        ];
        let distance = 5;
        let start = Instant::now();
        let result_option = csp_d_algorithm(&list, distance);
        let duration = start.elapsed();
        println!("Time elapsed is: {:?}", duration);
        assert_ne!(result_option, None);
    }
    #[test]
    fn test_csp_d_algorithm_timed_3_n6_l8_d5() {
        let list: Vec<String> = vec![
            "ACDACDDC".to_string(),
            "BBBDADBD".to_string(),
            "CDAADAAB".to_string(),
            "ABDCCDAD".to_string(),
            "BADCCDAD".to_string(),
            "CAABDBAA".to_string(),
        ];
        let distance = 5;
        let start = Instant::now();
        let result_option = csp_d_algorithm(&list, distance);
        let duration = start.elapsed();
        println!("Time elapsed is: {:?}", duration);
        assert_ne!(result_option, None);
    }
    #[test]
    fn test_csp_d_algorithm_timed_4_n7_l8_d5() {
        let list: Vec<String> = vec![
            "ACDACDDC".to_string(),
            "BBBDADBD".to_string(),
            "CDAADAAB".to_string(),
            "ABDCCDAD".to_string(),
            "BADCCDAD".to_string(),
            "CAABDBAA".to_string(),
            "ADBBDBAB".to_string(),
        ];
        let distance = 5;
        let start = Instant::now();
        let result_option = csp_d_algorithm(&list, distance);
        let duration = start.elapsed();
        println!("Time elapsed is: {:?}", duration);
        assert_ne!(result_option, None);
    }
    #[test]
    fn test_csp_d_algorithm_timed_5_n8_l8_d5() {
        let list: Vec<String> = vec![
            "ACDACDDC".to_string(),
            "BBBDADBD".to_string(),
            "CDAADAAB".to_string(),
            "ABDCCDAD".to_string(),
            "BADCCDAD".to_string(),
            "CAABDBAA".to_string(),
            "ADBBDBAB".to_string(),
            "ACABBAAA".to_string(),
        ];
        let distance = 5;
        let start = Instant::now();
        let result_option = csp_d_algorithm(&list, distance);
        let duration = start.elapsed();
        println!("Time elapsed is: {:?}", duration);
        assert_ne!(result_option, None);
    }

    #[test]
    fn test_csp_d_algorithm_timed_limit() {
        let list: Vec<String> = vec![
            "ABCDABCD".to_string(),
            "BCDABCDA".to_string(),
            "CDABCDAB".to_string(),
            "DABCDABC".to_string(),
        ];
        let distance = 5;
        let start = Instant::now();
        let result_option = csp_d_algorithm(&list, distance);
        let duration = start.elapsed();
        println!("Time elapsed is: {:?}", duration);
        assert_eq!(result_option, None);
    }

    #[test]
    fn test_csp_fixed_distance_timed_1_n4_l8_d5() {
        let list: Vec<String> = vec![
            "ACDACDDC".to_string(),
            "BBBDADBD".to_string(),
            "CDAADAAB".to_string(),
            "ABDCCDAD".to_string(),
        ];
        let distance = 5;
        let start = Instant::now();
        let result_option = csp_fixed_distance(&list, distance);
        let duration = start.elapsed();
        println!("Time elapsed is: {:?}", duration);
        assert_ne!(result_option, None);
    }
    #[test]
    fn test_csp_fixed_distance_timed_2_n5_l8_d5() {
        let list: Vec<String> = vec![
            "ACDACDDC".to_string(),
            "BBBDADBD".to_string(),
            "CDAADAAB".to_string(),
            "ABDCCDAD".to_string(),
            "BADCCDAD".to_string(),
        ];
        let distance = 5;
        let start = Instant::now();
        let result_option = csp_fixed_distance(&list, distance);
        let duration = start.elapsed();
        println!("Time elapsed is: {:?}", duration);
        assert_ne!(result_option, None);
    }
    #[test]
    fn test_csp_fixed_distance_timed_3_n6_l8_d5() {
        let list: Vec<String> = vec![
            "ACDACDDC".to_string(),
            "BBBDADBD".to_string(),
            "CDAADAAB".to_string(),
            "ABDCCDAD".to_string(),
            "BADCCDAD".to_string(),
            "CAABDBAA".to_string(),
        ];
        let distance = 5;
        let start = Instant::now();
        let result_option = csp_fixed_distance(&list, distance);
        let duration = start.elapsed();
        println!("Time elapsed is: {:?}", duration);
        assert_ne!(result_option, None);
    }
    #[test]
    fn test_csp_fixed_distance_timed_4_n7_l8_d5() {
        let list: Vec<String> = vec![
            "ACDACDDC".to_string(),
            "BBBDADBD".to_string(),
            "CDAADAAB".to_string(),
            "ABDCCDAD".to_string(),
            "BADCCDAD".to_string(),
            "CAABDBAA".to_string(),
            "ADBBDBAB".to_string(),
        ];
        let distance = 5;
        let start = Instant::now();
        let result_option = csp_fixed_distance(&list, distance);
        let duration = start.elapsed();
        println!("Time elapsed is: {:?}", duration);
        assert_ne!(result_option, None);
    }
    #[test]
    fn test_csp_fixed_distance_timed_5_n8_l8_d5() {
        let list: Vec<String> = vec![
            "ACDACDDC".to_string(),
            "BBBDADBD".to_string(),
            "CDAADAAB".to_string(),
            "ABDCCDAD".to_string(),
            "BADCCDAD".to_string(),
            "CAABDBAA".to_string(),
            "ADBBDBAB".to_string(),
            "ACABBAAA".to_string(),
        ];
        let distance = 5;
        let start = Instant::now();
        let result_option = csp_fixed_distance(&list, distance);
        let duration = start.elapsed();
        println!("Time elapsed is: {:?}", duration);
        assert_ne!(result_option, None);
    }
}
