pub mod lib_tests;

pub mod csp {
    use std::{collections::{HashSet, HashMap}, time::Instant, thread::current};

    pub fn csp_brute_force(list: &[String]) -> Option<String> {
        let current_distance = vec![0; list.len()];
        let vector = csp_brute_force_rec(list, String::from(""), &current_distance);
        let result = vector.iter().min_by_key(|tuple| tuple.1.iter().max());
        return match result {
            None => None,
            Some(x) => Some(x.0.clone()),
        };
    }

    pub fn csp_brute_force_rec(
        list: &[String],
        last_string: String,
        last_distances: &[usize],
    ) -> Vec<(String, Vec<usize>)> {
        if last_string.len() >= list.first().unwrap().len() {
            return vec![(last_string, last_distances.to_vec())];
        } else {
            let set = get_chars_from_index(list, last_string.len());
            let mut vector = Vec::new();
            for c in set {
                let mut appended_string = last_string.clone();
                appended_string.push(c);
                let new_distances = get_current_distances(list, &appended_string, last_distances);
                let mut result = csp_brute_force_rec(list, appended_string, &new_distances);
                vector.append(&mut result);
            }
            return vector;
        }
    }


    pub fn csp_fixed_distance(list: &[String], max_distance: usize) -> Option<String> {
        let new_distances = vec![0; list.len()];
        return csp_fixed_distance_rec(list, max_distance, &new_distances, String::from(""));
    }

    fn csp_fixed_distance_rec(
        list: &[String],
        max_distance: usize,
        last_distances: &[usize],
        last_string: String,
    ) -> Option<String> {
        let list_string_length = list.first().unwrap().len();
        if last_string.len() == list_string_length && check_distances(max_distance, last_distances)
        {
            return Some(last_string);
        } else if last_string.len() < list_string_length {
            let set = get_chars_from_index(list, last_string.len());
            for c in set {
                let mut appended_string = last_string.clone();
                appended_string.push(c);
                let new_distances = get_current_distances(list, &appended_string, last_distances);
                let result =
                    csp_fixed_distance_rec(list, max_distance, &new_distances, appended_string);
                match result {
                    Some(x) => return Some(x),
                    None => (),
                }
            }
            return None;
        } else {
            return None;
        }
    }

    fn get_chars_from_index(list: &[String], index: usize) -> Vec<char> {
        return list
            .iter()
            .map(|str| str.chars().collect::<Vec<char>>()[index])
            .collect::<HashSet<char>>()
            .into_iter()
            .collect::<Vec<char>>();
    }

    fn get_current_distances(
        list: &[String],
        current_string: &String,
        last_distances: &[usize],
    ) -> Vec<usize> {
        let last_string_index = current_string.len() - 1;
        let last_string_character =
            current_string.chars().collect::<Vec<char>>()[last_string_index];
        let mut new_distances = last_distances.to_vec();
        for i in 0..list.len() {
            let c = list[i].chars().collect::<Vec<char>>()[last_string_index];
            match c == last_string_character {
                true => (),
                false => {
                    new_distances[i] += 1;
                }
            }
        }
        return new_distances;
    }

    fn check_distances(max_distance: usize, current_distances: &[usize]) -> bool {
        return current_distances
            .iter()
            .all(|distance| *distance <= max_distance);
    }

    pub fn csp_d_algorithm(list: &[String], distance: usize) -> Option<String> {
        let kernelization_result = csp_d_algorithm_kernelization(list, distance);
        if !kernelization_result {
            return None
        }
        for s in list {
            let result = csp_d_algorithm_rec(s.clone(), list, distance, distance as i32);
            match result {
                Some(x) => return Some(x),
                None => (),
            }
        }

        return None;
    }

    fn csp_d_algorithm_kernelization(list: &[String], distance: usize) -> bool {
        let mut list_as_vector = list.to_vec();
        let mut vector_mapped = list_as_vector.iter().map(|s| s.chars().collect::<Vec<char>>()).collect::<Vec<Vec<char>>>();
        let mut dirty_column = 0;
        for l in 0..vector_mapped[0].len() {
            let mut mismatcher = HashSet::new();
            mismatcher.insert(vector_mapped[0][l]);
            for n in 1..vector_mapped.len() {
                if !mismatcher.contains(&vector_mapped[n][l]) {
                    mismatcher.insert(vector_mapped[n][l]);
                    dirty_column += 1;
                }
            }
        }
        if dirty_column <= distance * vector_mapped.len() {
            return true
        }
        return false
    }

    fn csp_d_algorithm_rec(
        current_string: String,
        list: &[String],
        max_distance: usize,
        delta_distance: i32,
    ) -> Option<String> {
        let strings_as_vector = list.to_vec();
        if delta_distance < 0 {
            return None;
        }

        // Note: Theoretically, this should be faster if l is long enough. But at the moment the one computing on the fly is faster.
        let hamming_distanzen = strings_as_vector.iter().map(|s| get_hamming_distance(&current_string, s)).collect::<Vec<usize>>();


        if *hamming_distanzen.iter().max().unwrap() > max_distance + delta_distance as usize {
            return None
        } 

        if hamming_distanzen.iter().max().unwrap() <= &max_distance {
            return Some(current_string);
        }


        // Note: This one is faster, if the string length is not really long
        // if strings_as_vector.iter().any(|s| {
        //     get_hamming_distance(&current_string, s) > max_distance + delta_distance as usize
        // }) {
        //     return None;
        // }
        // if strings_as_vector
        //     .iter()
        //     .all(|s| get_hamming_distance(&current_string, s) <= max_distance)
        // {
        //     return Some(current_string);
        // }

        let bigger_hamming_distance = strings_as_vector
            .iter()
            .filter(|s| get_hamming_distance(&current_string, s) > max_distance)
            .collect::<Vec<&String>>();
        for s in bigger_hamming_distance {
            let diff_indices = get_diff_indices(&current_string, s);
            let reduced_diff_indices = diff_indices
                .iter()
                .take(max_distance + 1)
                .collect::<Vec<&usize>>();
            let s_as_vector = s.clone().chars().collect::<Vec<char>>();
            for i in reduced_diff_indices {
                let mut new_string_vector: Vec<char> =
                    current_string.clone().chars().collect::<Vec<char>>();
                new_string_vector[*i] = s_as_vector[*i];
                let result = csp_d_algorithm_rec(
                    String::from_iter(new_string_vector.iter()),
                    list,
                    max_distance,
                    delta_distance - 1,
                );
                match result {
                    Some(s) => return Some(s),
                    None => (),
                }
            }
        }

        return None;
    }

    fn get_hamming_distance(s1: &String, s2: &String) -> usize {
        if s1.len() != s2.len() {
            panic!("The strings should have the same length");
        }
        let chars1 = s1.chars().collect::<Vec<char>>();
        let chars2 = s2.chars().collect::<Vec<char>>();
        let mut distance = 0;
        for i in 0..chars1.len() {
            if chars1[i] != chars2[i] {
                distance += 1;
            }
        }
        return distance;
    }
    fn get_diff_indices(s1: &String, s2: &String) -> Vec<usize> {
        if s1.len() != s2.len() {
            panic!("The strings should have the same length");
        }
        let mut result = Vec::new();
        let chars1 = s1.chars().collect::<Vec<char>>();
        let chars2 = s2.chars().collect::<Vec<char>>();
        for i in 0..chars1.len() {
            if chars1[i] != chars2[i] {
                result.push(i);
            }
        }

        return result;
    }



    #[cfg(test)]
    mod normal_test {
        use std::time::Instant;

        use crate::csp::{csp_brute_force, csp_fixed_distance, get_hamming_distance};

        #[test]
        fn test_csp_brute_force_1() {
            let list: Vec<String> = vec![
                "AAC".to_string(),
                "ABC".to_string(),
                "AAB".to_string(),
                "BAC".to_string(),
            ];
            let result_option = csp_brute_force(&list);
            let result = result_option.unwrap();
            assert_eq!(result, "AAC".to_string());
        }

        #[test]
        fn test_csp_brute_force_2() {
            let list: Vec<String> = vec![
                "AACAD".to_string(),
                "ABCAD".to_string(),
                "AABAD".to_string(),
                "BACDD".to_string(),
            ];
            let result_option = csp_brute_force(&list);
            assert_ne!(result_option, None);
        }

        #[test]
        fn test_csp_brute_force_3() {
            let list: Vec<String> = vec![
                "AACA".to_string(),
                "ABCA".to_string(),
                "AABA".to_string(),
                "BACD".to_string(),
            ];
            let result_option = csp_brute_force(&list);
            assert_ne!(result_option, None);
            let result = result_option.unwrap();
            assert_eq!(result, String::from("AACA"));
        }
        #[test]
        pub fn test_csp_brute_force_4() {
            let list: Vec<String> = vec![
                "AAACCCBBD".to_string(),
                "BBAACCBBD".to_string(),
                "AACCAACDD".to_string(),
                "DDAADDAAC".to_string(),
                "CCDDAACCA".to_string(),
                "AACAADDCC".to_string(),
                "CCAACCDDA".to_string(),
                "AADDAACCB".to_string(),
                "BABCCADDA".to_string(),
                "CCACADDBA".to_string(),
                "BBACADABA".to_string(),
            ];
            let start = Instant::now();
            let result_option = csp_brute_force(&list);
            let duration = start.elapsed();
            let result = result_option.clone().unwrap();
            let distances = list
                .iter()
                .map(|s| get_hamming_distance(&result, s))
                .collect::<Vec<usize>>();
            println!("String: {} with maximum distance: {:?}", result, distances);
            println!("Time elapsed is: {:?}", duration);
            assert_ne!(result_option, None);
        }

        #[test]
        fn test_csp_fixed_distance_1() {
            let list: Vec<String> = vec![
                "AAC".to_string(),
                "ABC".to_string(),
                "AAB".to_string(),
                "BAC".to_string(),
            ];
            let distance = 1;
            let result_option = csp_fixed_distance(&list, distance);
            let result = result_option.unwrap();
            assert_eq!(result, "AAC".to_string());
        }

        #[test]
        fn test_csp_fixed_distance_2() {
            let list: Vec<String> = vec![
                "AACAD".to_string(),
                "ABCAD".to_string(),
                "AABAD".to_string(),
                "BACDD".to_string(),
            ];
            let distance = 2;
            let result_option = csp_fixed_distance(&list, distance);
            assert_ne!(result_option, None);
        }

        #[test]
        fn test_csp_fixed_distance_3() {
            let list: Vec<String> = vec![
                "AACA".to_string(),
                "ABCA".to_string(),
                "AABA".to_string(),
                "BACD".to_string(),
            ];
            let distance = 1;
            let result_option = csp_fixed_distance(&list, distance);
            assert_eq!(result_option, None);
        }
        #[test]
        pub fn test_csp_fixed_distance_4() {
            let list: Vec<String> = vec![
                "AAACCCBBD".to_string(),
                "BBAACCBBD".to_string(),
                "AACCAACDD".to_string(),
                "DDAADDAAC".to_string(),
                "CCDDAACCA".to_string(),
                "AACAADDCC".to_string(),
                "CCAACCDDA".to_string(),
                "AADDAACCB".to_string(),
                "BABCCADDA".to_string(),
                "CCACADDBA".to_string(),
                "BBACADABA".to_string(),
            ];
            let distance = 6;
            let start = Instant::now();
            let result_option = csp_fixed_distance(&list, distance);
            let duration = start.elapsed();
            println!("Time elapsed is: {:?}", duration);
            assert_ne!(result_option, None);
        }
    }





    #[test]
    fn test_csp_d_algorithm_4() {
        let list: Vec<String> = vec![
            "AAACCCBBD".to_string(),
            "BBAACCBBD".to_string(),
            "AACCAACDD".to_string(),
            "DDAADDAAC".to_string(),
            "CCDDAACCA".to_string(),
            "AACAADDCC".to_string(),
            "CCAACCDDA".to_string(),
            "AADDAACCB".to_string(),
            "BABCCADDA".to_string(),
            "CCACADDBA".to_string(),
            "BBACADABA".to_string(),
        ];
        let distance = 6;
        let start = Instant::now();
        let result_option = csp_d_algorithm(&list, distance);
        let duration = start.elapsed();
        println!("Time elapsed is: {:?}", duration);
        assert_ne!(result_option, None);
    }

    
}
